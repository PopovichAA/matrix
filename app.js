/**
 * Основной объект 
 */
const App = {
	/**
	 * Обработка клика по кнопке генерации матрицы
	 */
	startButtonClickHandler() {
		App.process();
	},

	/**
	 * Запуск работы с матрицей
	 */
	process() {
		// создаем матрицу заданного размера
		let number = document.querySelector( "#number-input" ).value;		
		let matrix = Matrix.getMatrix( ( 2 * number - 1) );

		// отстроение матрицы
		let matrixBlock = document.querySelector( "#matrix-block" );
		matrixText = matrix.map( ( row ) => row.join( " " ) );
		matrixBlock.innerHTML = matrixText.join( "<br>" );

		// получение и отстроение элементов в спиралевидном порядке
		let spiralBlock = document.querySelector( "#spiral-matrix-block" );
		spiralBlock.innerHTML = Matrix.getSpiral( matrix );
	}
};
