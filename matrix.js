/**
 * Объект для работы с матрицами
 */
const Matrix = {
	
	/**
	 * Получение случайного числа от 10-99
	 * @returns {Number}
	 */
	getRandomNumber() {
		// ограничиваем случайное число двухзначным от 10 до 99
		let randomNumber = Math.random();
		randomNumber = ( randomNumber < 0.1 ) ? 0.1 : randomNumber;
		randomNumber = ( randomNumber < 0.99 ) ? randomNumber : 0.99;
		return Math.round( randomNumber * 100 );
	},

	/**
	 * Получение матрицы из случайных чисел заданного размера
	 * @params {Number} n - размер
	 * @returns {Array}
	 */
	getMatrix( n ) {
		// n должно быть целым числом
		n = parseInt( n );
		n = isNaN( n ) ? 5 : n;

		let rows = [];
		for( let i = 0; i < n; i++ ) {
			let cols = [];
			for( let j = 0; j < n; j++ ) { 
				cols.push( Matrix.getRandomNumber() );
			}
			rows.push( cols );
		}
		return rows;
	},

	/**
	 * Получение списка элементов в спиралевидном порядке
	 * @params {Array} matrix - матрица
	 */
	getSpiral( matrix ) {
		let result = [];

		// количество шагов по 1му направлению (растет через одно направление)
		let stepCount = 1;

		// направления движений
	    let directions = ["left", "bottom", "right", "top"];

	    // находим центр и добавляем его в результат
	    let col = Math.floor( matrix.length / 2 );
	    let row = Math.floor( matrix.length / 2 );
		result.push( matrix[row][col] );		

		// флаг о том, что больше нечего добавлять
     	matrixIsEnd = false;     	

     	// цикл до окончания матрицы
 		for( let directionIndex = 0; !matrixIsEnd; directionIndex++ ) {
			// определяем направление по индексу
			let index = directionIndex % directions.length;
			let direction = directions[index];

		    for( let step = 0; step < stepCount; step++ ) {
		    	// в зависимости от направления изменяем координаты
		    	switch( direction ) {
		    		case "left":
		    			col--;
		    			break;
	    			case "bottom":
		    			row++;
		    			break;
	    			case "right":
		    			col++;
		    			break;
	    			case "top":
		    			row--;
		    			break;
	    			default:
	    				break;
		    	}		    	

	    		// если значение есть в массиве, то добавляем в результат,
	    		// иначе матрица закончилась
		    	if( matrix[row] && ( matrix[row][col] >= 0 ) ) {
					result.push( matrix[row][col] );	    		
		    	} else {
		    		matrixIsEnd = true;
		    		break;
		    	}	    	
		    }

	    	// увеличиваем количество шагов
		    if( directionIndex % 2 == 1 ) {
		    	stepCount++;
		    }		    
 		}    

	   	return result.join( " " );
	}
};